<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$app->get('/', 'ChimpController@getIndex');
$app->get('/list/all', 'ChimpController@getLists');
$app->get('/list/create', 'ChimpController@getCreate');
$app->post('/list/store', 'ChimpController@postStore');
$app->get('/list/show/{id}', 'ChimpController@getShow');
$app->get('/list/edit/{id}', 'ChimpController@getEdit');
$app->put('/list/update/{id}', 'ChimpController@putUpdate');
$app->delete('/list/delete', 'ChimpController@deleteDestroy');

$app->post('/member/store', 'ChimpController@postStoreMember');
$app->put('/member/update', 'ChimpController@putUpdateMember');
$app->delete('/member/delete', 'ChimpController@deleteDestroyMember');