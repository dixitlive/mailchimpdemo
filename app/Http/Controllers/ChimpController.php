<?php

namespace App\Http\Controllers;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;

class ChimpController extends Controller{

    /**
     * MailChimp Demo A/C
     * Email: dixiton+loyaltycorp@gmail.com
     * Username: lcdemo12345
     * Pass: Apple12345$
     */

    //api key
    private $apiKey = 'e4127008b81866ab7884d5038c464d40-us14';
    private $apiEndpoint = 'https://us14.api.mailchimp.com/3.0/';
    public $verify_ssl = true;
    private $request_successful = false;
    private $last_error         = '';
    private $last_response      = array();
    private $last_request       = array();

    /**
     * Performs the underlying HTTP request. Not very exciting.
     * @param  string $http_verb The HTTP verb to use: get, post, put, patch, delete
     * @param  string $method The API method to be called
     * @param  array $args Assoc array of parameters to be passed
     * @param int $timeout
     * @return collection|false Assoc array of decoded result
     * @throws \Exception
     */
    private function makeRequest($http_verb, $method, $args = array(), $timeout = 10)
    {
        if (!function_exists('curl_init') || !function_exists('curl_setopt')) {
            throw new \Exception("cURL support is required, but can't be found.");
        }
        $url = $this->apiEndpoint . '/' . $method;
        $this->last_error         = '';
        $this->request_successful = false;
        $response                 = array('headers' => null, 'body' => null);
        $this->last_response      = $response;
        $this->last_request = array(
            'method'  => $http_verb,
            'path'    => $method,
            'url'     => $url,
            'body'    => '',
            'timeout' => $timeout,
        );
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Accept: application/vnd.api+json',
            'Content-Type: application/vnd.api+json',
            'Authorization: apikey ' . $this->apiKey
        ));
        curl_setopt($ch, CURLOPT_USERAGENT, 'DrewM/MailChimp-API/3.0 (github.com/drewm/mailchimp-api)');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, $this->verify_ssl);
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_0);
        curl_setopt($ch, CURLOPT_ENCODING, '');
        curl_setopt($ch, CURLINFO_HEADER_OUT, true);
        switch ($http_verb) {
            case 'post':
                curl_setopt($ch, CURLOPT_POST, true);
                $this->attachRequestPayload($ch, $args);
                break;
            case 'get':
                $query = http_build_query($args, '', '&');
                curl_setopt($ch, CURLOPT_URL, $url . '?' . $query);
                break;
            case 'delete':
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'DELETE');
                break;
            case 'patch':
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PATCH');
                $this->attachRequestPayload($ch, $args);
                break;
            case 'put':
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
                $this->attachRequestPayload($ch, $args);
                break;
        }
        $response['body']    = curl_exec($ch);
        $response['headers'] = curl_getinfo($ch);
        if (isset($response['headers']['request_header'])) {
            $this->last_request['headers'] = $response['headers']['request_header'];
        }
        if ($response['body'] === false) {
            $this->last_error = curl_error($ch);
        }
        curl_close($ch);
        $formattedResponse = $this->formatResponse($response);
        return new Collection($formattedResponse);
    }

    /**
     * Encode the data and attach it to the request
     * @param   resource $ch cURL session handle, used by reference
     * @param   array $data Assoc array of data to attach
     */
    private function attachRequestPayload(&$ch, $data)
    {
        $encoded = json_encode($data);
        $this->last_request['body'] = $encoded;
        curl_setopt($ch, CURLOPT_POSTFIELDS, $encoded);
    }

    /**
     * Decode the response and format any error messages for debugging
     * @param array $response The response from the curl request
     * @return array|false    The JSON decoded into an array
     */
    private function formatResponse($response)
    {
        $this->last_response = $response;
        if (!empty($response['body'])) {
            return json_decode($response['body'], true);
        }
        return false;
    }

    /**
     * Prepare list form input
     * @param array $input
     * @return array $input
    */
    public function prepareInput($input)
    {
        $input['contact'] = [
            'company' => array_pull($input, 'company'),
            'address1' => array_pull($input, 'address1'),
            'address2' => array_pull($input, 'address2'),
            'city' => array_pull($input, 'city'),
            'state' => array_pull($input, 'state'),
            'zip' => array_pull($input, 'zip'),
            'country' => array_pull($input, 'country'),
        ];
        $input['permission_reminder'] = 'yes';
        $input['email_type_option'] = true;
        $input['campaign_defaults'] = [
            'from_name' => 'Demo',
            'from_email' => 'dixiton+loyaltycorp@gmail.com',
            'subject' => 'New Message',
            'language' => 'en',
        ];

        return $input;
    }

    /**
     * App main welcome page
     *
    */
    public function getIndex()
    {
        return view('index');
    }

    /**
     * Get lists
     * @return Response
     */
    public function getLists()
    {
        $items = $this->makeRequest('get', 'lists');
        //return $items;
        return view('list', compact('items'));
    }

    /**
     *  Create a new list
    */
    public function getCreate()
    {
        return view('create');
    }

    /**
     * Store new list
     * @param Request $request
     * @return Response
    */
    public function postStore(Request $request)
    {
        $list = $this->makeRequest('post', 'lists', $this->prepareInput($request->all()));
        return redirect('/list/all');
    }

    /**
     * Show list
     * @param string $id
     * @return Response
    */
    public function getShow($id)
    {
        $item = $this->makeRequest('get', 'lists/'.$id);
        $item['info'] = $this->makeRequest('get', 'lists/'.$id.'/members');
//        return $item;
        return view('show', compact('item'));
    }

    /**
     * Edit List
     * @param string $id
     * @return Response
    */
    public function getEdit($id)
    {
        $item = $this->makeRequest('get', 'lists/'.$id);
        return view('edit', compact('item'));
    }

    /**
     * Update specific list record
     * @param string $id
     * @param Request $request
     * @return Response
    */
    public function putUpdate($id, Request $request)
    {
        $item = $this->makeRequest('patch', 'lists/'.$id, $this->prepareInput($request->all()));
        return redirect('/list/edit/'.$id);
    }

    /**
     * Delete list
     * @param Request $request
     * @return array
    */
    public function deleteDestroy(Request $request)
    {
        $input = $request->all();
        $this->makeRequest('delete', 'lists/'.$input['id']);
        return [
            'success' => true,
        ];
    }

    /**
     * Add new member to store
     * @param Request $request
     * @return Response
    */
    public function postStoreMember(Request $request)
    {
        $input = $request->all();
            $member = $this->makeRequest('post', 'lists/' . $input['id'] . '/members', [
            'email_address' => $input['email_address'],
            'status' => 'subscribed'
        ]);

        //return $member;
        return redirect('/list/show/'.$input['id']);
    }

    /**
     * Update member on the list
     * @param Request $request
     * @return array
    */
    public function putUpdateMember(Request $request)
    {
        $input = $request->all();
        $update = $this->makeRequest('patch', 'lists/'.$input['list_id'].'/members/'.$input['member_id'], [
            'status' => $input['status']
        ]);
        return redirect('/list/show/' . $input['list_id']);
    }

    /**
     * Delete member from list
     * @param Request $request
     * @return array
    */
    public function deleteDestroyMember(Request $request)
    {
        $input = $request->all();
        $this->makeRequest('delete', 'lists/'.$input['list_id'].'/members/'.$input['member_id']);
        return [
            'success' => true,
        ];
    }
}