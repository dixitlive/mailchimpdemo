@extends('app')
@section('title')
    @parent
    Welcome to MailChimp Demo
@stop
@section('content')
    <div class="col-md-12">
        <h1 class="text-center">Welcome to MailChimp Demo</h1>
        <h3 class="text-center lead">Manage your MailChimp email lists like a pro!</h3>
    </div>
@stop
