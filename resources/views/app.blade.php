<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8"/>

    <title>
        @section('title')
        @show
    </title>

    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <link rel="stylesheet" href="/lib/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="/lib/bootstrap/dist/css/bootstrap-theme.min.css">
</head>
<body>
<nav class="navbar" style="margin-top: -1px;">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#toggle-menu" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand brand-color" href="/">MailChimp Demo</a>
        </div>
        <div class="collapse navbar-collapse" id="toggle-menu">
                <ul class="nav navbar-nav">
                    <li>
                        <a href="/list/create">Create new list</a>
                    </li>
                    <li>
                        <a href="/list/all">View lists</a>
                    </li>
                </ul>
        </div>
    </div>
</nav>
<div class="container">
    @yield('content')
</div>
<script src="/lib/jquery/dist/jquery.min.js"></script>
</body>
</html>
