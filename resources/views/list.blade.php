@extends('app')
@section('title')
    @parent
    Create new list
@stop
@section('content')
    <table class="table">
        <thead>
        <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Company</th>
            <th>Created</th>
            <th>Actions</th>
        </tr>
        </thead>
        <tbody>
        @foreach($items['lists'] as $item)
            <tr>
            <td>{{ $item['id'] }}</td>
            <td>{{ $item['name'] }}</td>
            <td>{{ $item['contact']['company'] }}</td>
            <td>{{ $item['date_created'] }}</td>
            <td>
                <a href="/list/show/{{$item['id']}}" class="btn btn-default">View</a>
                <a href="/list/edit/{{$item['id']}}" class="btn btn-default">Edit</a>
                <a href="javascript:deleteList('{{ $item['id'] }}')" class="btn btn-danger">Delete</a>
            </td>
            </tr>
        @endforeach
        </tbody>
    </table>


    <script>
        //delete the list
        deleteList = function(id){
            $.ajax({
                url: '/list/delete/',
                type: 'DELETE',
                data : {
                    id : id
                },
                success: function(response) {
                    if(response.success){
                        location.reload();
                    }
                }
            });
        }
    </script>
@stop
