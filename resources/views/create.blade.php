@extends('app')
@section('title')
    @parent
    Create new list
@stop
@section('content')
    <div class="col-md-6 col-md-offset-3">
        <div class="panel panel-default">
            <div class="panel-heading">Create new list</div>
            <div class="panel-body">

                <form class="form-horizontal" role="form" method="POST" action="{{ url('/list/store') }}">

                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="name" class="col-md-4 control-label">List name</label>
                            <div class="col-md-8">
                                <input id="name" type="text" class="form-control" name="name" required autofocus>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="company" class="col-md-4 control-label">Company</label>
                            <div class="col-md-8">
                                <input id="company" type="text" class="form-control" name="company" required>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="address1" class="col-md-4 control-label">Address - 1</label>
                            <div class="col-md-8">
                                <input id="address1" type="text" class="form-control" name="address1" required>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="address2" class="col-md-4 control-label">Address - 2</label>
                            <div class="col-md-8">
                                <input id="address2" type="text" class="form-control" name="address2">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="city" class="col-md-4 control-label">City</label>
                            <div class="col-md-8">
                                <input id="city" type="text" class="form-control" name="city" required autofocus>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="state" class="col-md-4 control-label">State</label>
                            <div class="col-md-8">
                                <input id="state" type="text" class="form-control" name="state" required>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="zip" class="col-md-4 control-label">Postcode</label>
                            <div class="col-md-8">
                                <input id="zip" type="text" class="form-control" name="zip" required>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="country" class="col-md-4 control-label">Country</label>
                            <div class="col-md-8">
                                <input id="country" type="text" class="form-control" name="country" required>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12  col-md-offset-5">
                        <div class="form-group">
                            <div class="col-md-6">
                                <button type="submit" class="btn btn-primary">
                                    Create
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@stop
