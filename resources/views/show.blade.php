@extends('app')
@section('title')
    @parent
    List : {{$item['name']}}
@stop
@section('content')

    <div class="col-md-12">
        <h1>List: {{ $item['name'] }}</h1>
    </div>

    <div class="col-md-6 col-md-offset-3">
        <div class="panel panel-primary">
            <div class="panel-heading">Add new member to the list</div>
            <div class="panel-body">
                <form class="form-horizontal" role="form" method="POST" action="{{ url('/member/store') }}">

                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="email_address" class="col-md-4 control-label">Email address</label>
                            <div class="col-md-8">
                                <input id="email_address" type="email" class="form-control" name="email_address" required autofocus>
                            </div>
                        </div>
                    </div>

                    <input type="hidden" name="id" value="{{$item['id']}}">
                    <div class="col-md-12  col-md-offset-5">
                        <div class="form-group">
                            <div class="col-md-6">
                                <button type="submit" class="btn btn-primary">
                                    Add
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="col-md-12">
        <h3>Members</h3>
        <table class="table">
            <thead>
            <tr>
                <th>Member ID</th>
                <th>Email</th>
                <th>Status</th>
                <th>Actions</th>
            </tr>
            </thead>
            <tbody>
            @if(count($item['info']['members']))
                @foreach($item['info']['members'] as $member)
                    <tr>
                        <td>{{ $member['id'] }}</td>
                        <td>{{ $member['email_address'] }}
                            <form class="form-horizontal" role="form" method="POST" action="{{ url('/member/update/') }}" id="{{ $member['id'] }}">
                                <input type="hidden" name="list_id" value="{{ $member['list_id'] }}">
                                <input type="hidden" name="member_id" value="{{ $member['id'] }}">
                                <input type="hidden" name="status" value="{{ $member['status'] == 'subscribed' ? 'unsubscribed' : 'subscribed' }}">
                                <input type="hidden" name="_method" value="PUT">
                            </form>
                        </td>
                        <td>{{ $member['status'] }}</td>
                        <td>
                            <a href="#" onclick="submitForm('{{ $member['id'] }}')" class="btn {{$member['status'] == 'unsubscribed' ? 'btn-success' : 'btn-warning' }} ">{{$member['status'] == 'subscribed' ? 'Unsubscribe' : 'Subscribe' }}</a>
                            <a href="javascript:deleteMember('{{ $item['id'] }}','{{ $member['id'] }}')" class="btn btn-danger">Remove</a>
                        </td>
                    </tr>
                @endforeach
            @endif
            </tbody>
        </table>
    </div>

    <script>
        //delete the list
        deleteList = function(id){
            $.ajax({
                url: '/list/delete/',
                type: 'DELETE',
                data : {
                    id : id
                },
                success: function(response) {
                    if(response.success){
                        location.reload();
                    }
                }
            });
        };

        //update member
        submitForm = function(id){
            $('#'+id).submit()
        };

        //delete member from list
        deleteMember = function(listId, memberId){
            $.ajax({
                url: '/member/delete/',
                type: 'DELETE',
                data : {
                    'list_id' : listId,
                    'member_id' : memberId
                },
                success: function(response) {
                    if(response.success){
                        location.reload();
                    }
                }
            });
        };
    </script>
@stop
